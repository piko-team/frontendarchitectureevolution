var ClickCounterViewModel = function() {
    this.model = ko.observable(0);

    this.increment = function() {
        this.model(this.model() + 1);
    };

    this.decrement = function() {
        this.model(this.model() - 1);
    };
};

ko.applyBindings(new ClickCounterViewModel());
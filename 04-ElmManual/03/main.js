function app({state, view, actions}) {
    const root = document.body.appendChild(document.createElement('div'));

    function mountedView(state, actions) {
        yo.update(root, view(state, actions));
    }

    Object.keys(actions).forEach(function (actionName) {
        const originalAction = actions[actionName];
        actions[actionName] = function () {
            state = originalAction(state);
            mountedView(state, actions);
        }
    });

    mountedView(state, actions);
}

app({
    state: 0,
    actions: {
        increment(state) {
            return state + 1;
        },
        decrement(state) {
            return state - 1;
        }
    },
    view: (state, actions) => yo`
            <div>
                <h1>${state}</h1>
                <button onclick=${actions.increment}>+</button>
                <button onclick=${actions.decrement}>-</button>
            </div>
            `
});





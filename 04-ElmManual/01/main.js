// model
let state = 0;

// actions/update/reduce
const actions = {
    increment() {
        state += 1;
        view(state);
    },
    decrement() {
        state -= 1;
        view(state);
    }
};

// view
function view() {
    root.innerHTML = `
    <div>
        <h1>${state}</h1>
        <button onclick=actions.increment()>+</button>
        <button onclick=actions.decrement()>-</button>
    </div>
    `;
}


const root = document.body.appendChild(document.createElement('div'));

view(state);
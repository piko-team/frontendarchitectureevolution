// model
let state = 0;

// actions/update/reduce
const actions = {
    increment() {
        state += 1;
        view();
    },
    decrement() {
        state -= 1;
        view();
    }
};

// view
function view() {
    yo.update(root, yo`
    <div>
        <h1>${state}</h1>
        <button onclick=${actions.increment}>+</button>
        <button onclick=${actions.decrement}>-</button>
    </div>
    `);
}


const root = document.body.appendChild(document.createElement('div'));

view();
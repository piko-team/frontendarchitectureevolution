# Evolving Frontend Architecture

## 01 - Model in DOM

* Query and update DOM directly to update model/state.
* Quick to start and hack
* Deferred effort, difficult to reason about at scale
* Difficult to test because of UI/logic mixing
* fine grain DOM updates
* Low level DOM API or jQuery facade over DOM API

## 02 - Model-View-Presenter

* Model - data and business logic, View - encapsulates DOM access, Presenter - is a
mediator between model and view
* Model and Presenter are testable independently of the View
* Code is very explicit but also quite verbose

## 03 - Model-View-ViewModel

* usually requires a framework/library (e.g. knockout.js)
* based on two-way data binding
* ViewModel changes trigger (partial) View updates
* View actions trigger ViewModel updates
* very declarative
* may get difficult to reason about at scale when updates cascade in both directions

## 04 - Elm Architecture

* used in different ecosystems (React/Redux, Elm, hyperapp, choo)
* 3 main concepts: model/state, view, update/actions
* re-render the whole view on every update (Virtual DOM to make it efficient)
* view in JS (JSX or tagged template literals) and represented as a function of state
* separation of concerns, not technologies
* one way data flow: actions change model/state and view is automatically updated
based on the new model/state

const { h, app } = hyperapp;
/** @jsx h */

app({
  state: { counter: 0 },
  view: state => actions =>
    <div>
      <h1>
        {state.counter}
      </h1>
      <button onclick={actions.increment}>+</button>
      <button onclick={actions.decrement} disabled={state.counter <= 0}>
        -
      </button>
    </div>,
  actions: {
    increment: () => state => ({ counter: state.counter + 1 }),
    decrement: () => state => ({ counter: state.counter - 1 })
  }
});

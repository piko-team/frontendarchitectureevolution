const model = {
    _count: 0,
    increment() {
        model._count++;
    },
    decrement() {
        model._count--;
    },
    count() {
        return model._count;
    }
};

const view = {
    updateCount(value) {
        document.querySelector('.count').innerHTML = value;
    },
    onIncrement(handler) {
        document.querySelector('.increment').addEventListener('click', handler);
    },
    onDecrement(handler) {
        document.querySelector('.decrement').addEventListener('click', handler);
    }
};

const presenter = {
    model: model,
    view: view,
    increment() {
        presenter.model.increment();
        presenter.view.updateCount(presenter.model.count());
    },
    decrement() {
        presenter.model.decrement();
        presenter.view.updateCount(presenter.model.count());
    },
    init() {
        presenter.view.onIncrement(presenter.increment);
        presenter.view.onDecrement(presenter.decrement);
        presenter.view.updateCount(presenter.model.count());
    }
};

presenter.init();

var incrementButton = document.querySelector('.increment');
var countField = document.querySelector('.count');
var decrementButton = document.querySelector('.decrement');

incrementButton.addEventListener('click', function() {
    countField.innerHTML = parseInt(countField.innerHTML) + 1;
});


decrementButton.addEventListener('click', function() {
    countField.innerHTML = parseInt(countField.innerHTML) - 1;
});